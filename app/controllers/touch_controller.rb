class TouchController < ApplicationController
    def create
        @newrepo = Repo.new
        @newrepo.author = params[:author]
        @newrepo.name = params[:name]
        @newrepo.text = params[:text]
        @newrepo.save
    end

    def update
        @exrepo = Repo.find_by(author: params[:author], name: params[:name])
        @exrepo = @exrepo.text + params[:text]
        @exrepo.save
    end

    def destroy
        @exrepo = Repo.find_by(author: params[:author], name: params[:name])
        @exrepo.destroy
    end
end