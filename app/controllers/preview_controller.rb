class PreviewController < ApplicationController
    def create
        @screen = Screen.new
        @screen.current = params[:current]
        @screen.capture = params[:capture]
        @screen.agent = Agent.new
        @screen.save
    end
end
